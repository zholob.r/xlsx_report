import openpyxl
import pandas as pd
import os
from datetime import datetime


class Report:
    def __init__(self):
        """ self.main_file, self.data_file - зберігають відповідний перелік назв файлів які нас цікавлять
        self.name_list:  тут будуть зберігатись унікальні назви
        self.id_key_company:  тут буде словник  ІД - ім'я
        """
        self.main_file, self.data_file = Report.get_file_name()
        self.name_list, self.id_name = Report.get_data_from_main(self.main_file)
        # словник з ключами назвами компаній і значеннями - кількість
        # [sms, email]
        self.key_dict = {key: [0, 0] for key in self.name_list}

    @staticmethod
    def get_file_name():
        """
        зберігаємо до атрибутів класів настуне:
        main_file: файл звідки тягнемо компанії та ід
        :return: кортеж назви головного файлу та списку фаллів де дивимось данні
        """
        current_dir = os.path.dirname(os.path.abspath(__file__))
        files = os.listdir(current_dir)
        data_file = []
        main_file = None
        for file_name in files:
            if file_name.lower().find('main') >= 0:
                main_file = file_name
            elif file_name.lower().find('sms') >= 0 or file_name.lower().find('email') >= 0:
                data_file.append(file_name)

        return main_file, data_file

    @staticmethod
    def get_data_from_main(main_file):
        """
        :param main_file: - назва файлу де беремо ІД та ім'я
        :return:  ліст унікальних назв кампаній nа словник ІД : ім'я
        """
        df = pd.read_excel(main_file, engine='openpyxl')
        name_list = []  # тут будуть зберігатись унікальні назви
        id_name = {}  # тут буде словник  ІД - ім'я
        for index, row in df.iterrows():
            key = row['id']
            value = row['name']
            id_name[key] = value
            name_list.append(value)
        # робимо ліст унікальних назв кампаній
        name_list = list(set(name_list))

        return name_list, id_name

    def load_data_from_file(self):
        for file in self.data_file:
            time_start = datetime.now()
            df = pd.read_excel(file, engine='openpyxl')
            for idx, row in df.iterrows():
                id = row['id']
                if id in self.id_name:
                    count = row['count']
                    if file.lower().find('sms') >= 0:
                        self.key_dict[self.id_name.get(id)][0] += count
                    else:
                        self.key_dict[self.id_name.get(id)][1] += count

            print(f'Файл {file} оброблено за {datetime.now() - time_start}')

    def save_data_in_file(self):
        wb = openpyxl.Workbook()
        sheet = wb.active
        sheet.cell(row=1, column=1).value = 'name'
        sheet.cell(row=1, column=3).value = 'sms'
        sheet.cell(row=1, column=4).value = 'email'

        row = 2
        for key, value in self.key_dict.items():
            if value[0] > 0 or value[1] > 0:
                sheet.cell(row=row, column=1).value = key
                sheet.cell(row=row, column=3).value = value[0]
                sheet.cell(row=row, column=4).value = value[1]
                row += 1
        wb.save('report.xlsx')


def main():
    rep = Report()
    rep.load_data_from_file()
    rep.save_data_in_file()


if __name__ == '__main__':
    main()
    input()
